import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rlogical/utils/app_config.dart' as config;
import 'package:rlogical/utils/constants.dart';
import 'package:rlogical/utils/globalElements.dart';

import 'checkCalender.dart';

class UserDetail extends StatefulWidget {
  @override
  _UserDetail createState() => _UserDetail();
}

class _UserDetail extends State<UserDetail> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            title: Container(
              padding: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'John Smith',
                          style: headerTextForIdentification,
                        ),
                        Text(
                          'personal Trainer',
                          style: subTitleHeader,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              IconButton(
                padding: EdgeInsets.all(10.0),
                icon: Icon(
                  Icons.notifications,
                  color: Colors.grey,
                ),
                onPressed: () {
                  // Implement navigation to shopping cart page here...
                  print('Click Action3');
                },
              ),
              InkWell(
                  onTap: () {
                    print('Click Profile Pic');
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.network(
                      "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
                      height: 50,
                      width: 50,
                    ),
                  ))
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.face), title: Text('ADD CLIENT')),
            BottomNavigationBarItem(
                icon: Icon(Icons.send), title: Text('SEND MESSAGE')),
            BottomNavigationBarItem(
                icon: Icon(Icons.menu), title: Text('MY CLIENTS')),
          ],
          currentIndex: 0,
          fixedColor: Colors.deepPurple,
          onTap: (value) {},
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 3,
              ),
              Positioned(
                width: config.App(context).appWidth(100),
                child: Container(
                  height: MediaQuery.of(context).size.height / 2,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: config.App(context).appHeight(20),
                        width: config.App(context).appWidth(100),
                        margin: EdgeInsets.only(top: 10, left: 24, right: 24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            CardBlock(
                              headingText: "FOOD LOG",
                              click: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CheckCalender()),
                                );
                              },
                            ),
                            CardBlock(
                              headingText: "ACTIVITY LOG",
                              click: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CheckCalender()),
                                );
                              },
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: config.App(context).appWidth(100),
                        height: config.App(context).appHeight(20),
                        margin: EdgeInsets.only(top: 10, left: 24, right: 24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            CardBlock(
                              headingText: "WEIGHT CHART",
                              click: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CheckCalender()),
                                );
                              },
                            ),
                            CardBlock(
                              headingText: "CALL LOG",
                              click: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CheckCalender()),
                                );
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
