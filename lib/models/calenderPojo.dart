import 'calenderData.dart';

class Calender {
  // String error;
  // Message message;
  CalenderData data;

  Calender({this.data});

  Calender.fromJson(Map<String, dynamic> json) {
    // error = json['Error'];
    // message =
    // json['Message'] != null ? new Message.fromJson(json['Message']) : null;
    data =
        json['Data'] != null ? new CalenderData.fromJson(json['Data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    // data['Error'] = this.error;
    // if (this.message != null) {
    //   data['Message'] = this.message.toJson();
    // }
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    return data;
  }
}
