class BookedEventSessionDateListResModel {
  String endDateTime;
  String eventSessionDate;
  int eventSessionId;
  int isAvailable;
  int isBooked;
  int isCanceled;
  int isClassSessionFull;
  String startDateTime;

  BookedEventSessionDateListResModel(
      {this.endDateTime,
        this.eventSessionDate,
        this.eventSessionId,
        this.isAvailable,
        this.isBooked,
        this.isCanceled,
        this.isClassSessionFull,
        this.startDateTime});

  BookedEventSessionDateListResModel.fromJson(Map<String, dynamic> json) {
    endDateTime = json['EndDateTime'];
    eventSessionDate = json['EventSessionDate'];
    eventSessionId = json['EventSessionId'];
    isAvailable = json['IsAvailable'];
    isBooked = json['IsBooked'];
    isCanceled = json['IsCanceled'];
    isClassSessionFull = json['IsClassSessionFull'];
    startDateTime = json['StartDateTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EndDateTime'] = this.endDateTime;
    data['EventSessionDate'] = this.eventSessionDate;
    data['EventSessionId'] = this.eventSessionId;
    data['IsAvailable'] = this.isAvailable;
    data['IsBooked'] = this.isBooked;
    data['IsCanceled'] = this.isCanceled;
    data['IsClassSessionFull'] = this.isClassSessionFull;
    data['StartDateTime'] = this.startDateTime;
    return data;
  }
}