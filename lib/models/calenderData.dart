import 'bookedEventSessionDataList.dart';

class CalenderData {
  List<BookedEventSessionDateListResModel> bookedEventSessionDateListResModel;

  CalenderData({this.bookedEventSessionDateListResModel});

  CalenderData.fromJson(List<dynamic> json) {
    // if (json['BookedEventSessionDateListResModel'] != null) {
    bookedEventSessionDateListResModel =
        new List<BookedEventSessionDateListResModel>();
    bookedEventSessionDateListResModel = json
        .map((e) => BookedEventSessionDateListResModel.fromJson(e))
        .toList();
    /*json['BookedEventSessionDateListResModel'].forEach((v) {
        bookedEventSessionDateListResModel
            .add(new BookedEventSessionDateListResModel.fromJson(v));
      });*/
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.bookedEventSessionDateListResModel != null) {
      data['BookedEventSessionDateListResModel'] = this
          .bookedEventSessionDateListResModel
          .map((v) => v.toJson())
          .toList();
    }
    return data;
  }
}
