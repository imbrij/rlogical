import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rlogical/models/recentDummyModel.dart';
import 'package:rlogical/userDetail.dart';
import 'package:rlogical/utils/constants.dart';

class RecentListPage extends StatefulWidget {
  @override
  _RecentListPage createState() => new _RecentListPage();
}

class _RecentListPage extends State<RecentListPage> {
  List<RecentDummyModels> recentDummyData = List();

  @override
  void initState() {
    super.initState();

    setupDummyData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            title: Container(
              padding: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'John Smith',
                          style: headerTextForIdentification,
                        ),
                        Text(
                          'personal Trainer',
                          style: subTitleHeader,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              IconButton(
                padding: EdgeInsets.all(10.0),
                icon: Icon(
                  Icons.notifications,
                  color: Colors.grey,
                ),
                onPressed: () {
                  // Implement navigation to shopping cart page here...
                  print('Click Action3');
                },
              ),
              InkWell(
                  onTap: () {
                    print('Click Profile Pic');
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.network(
                      "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
                      height: 50,
                      width: 50,
                    ),
                  ))
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.face), title: Text('ADD CLIENT')),
            BottomNavigationBarItem(
                icon: Icon(Icons.send), title: Text('SEND MESSAGE')),
            BottomNavigationBarItem(
                icon: Icon(Icons.menu), title: Text('MY CLIENTS')),
          ],
          currentIndex: 0,
          fixedColor: Colors.deepPurple,
          onTap: (value) {},
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView.builder(
            itemCount: recentDummyData.length,
            itemBuilder: (context, index) {
              return listItem(recentDummyData[index]);
            },
          ),
        ),
      ),
    );
  }

  Widget listItem(RecentDummyModels recentDummyModels) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => UserDetail()),
        );
      },
      child: Card(
        shadowColor: Colors.grey,
        child: Container(
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.all(5),
          /* decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
        ),*/
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Image.network(
                  recentDummyModels.imageUrl,
                  height: 50,
                  width: 50,
                ),
              ),
              Expanded(
                flex: 4,
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(recentDummyModels.name),
                      Row(
                        children: <Widget>[
                          Text(recentDummyModels.kCal),
                          recentDummyModels.isPlusCal
                              ? Icon(
                                  Icons.add_box,
                                  color: Colors.yellow,
                                  size: 20,
                                )
                              : Icon(
                                  Icons.indeterminate_check_box,
                                  size: 20,
                                )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              /* Expanded(
              flex: 1,
              child:*/
              Container(
                width: 35,
                height: 35,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(22)),
                ),
                child: Text(
                  "${recentDummyModels.count}",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              // ),
              Expanded(
                flex: 1,
                child: recentDummyModels.isFlag
                    ? Icon(
                        Icons.flag,
                        color: Colors.red,
                      )
                    : Container(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void setupDummyData() {
    recentDummyData.add(RecentDummyModels(
        "Kathy Robert",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "489 Kcal",
        false,
        0,
        false));
    recentDummyData.add(RecentDummyModels(
        "Jessica Phillips",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "120 Kcal",
        false,
        0,
        false));
    recentDummyData.add(RecentDummyModels(
        "Amy Brown",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "346 Kcal",
        true,
        7,
        true));
    recentDummyData.add(RecentDummyModels(
        "Bruce Shield",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "350 Kcal",
        false,
        0,
        false));
    recentDummyData.add(RecentDummyModels(
        "Bianca Doyle Robert",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "646 Kcal",
        true,
        0,
        false));
    recentDummyData.add(RecentDummyModels(
        "Kathy Robert",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "146 Kcal",
        true,
        0,
        true));
    recentDummyData.add(RecentDummyModels(
        "Tony Lee Robert",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "350 Kcal",
        false,
        0,
        true));
    recentDummyData.add(RecentDummyModels(
        "Kathy Robert",
        "https://miro.medium.com/max/560/1*MccriYX-ciBniUzRKAUsAw.png",
        "350 Kcal",
        false,
        0,
        false));
  }
}
