import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app_config.dart';

class CardBlock extends StatelessWidget {
  CardBlock({this.headingText, this.image, this.click});

  final headingText;
  final image;
  Function click;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: click,
      child: Container(
        width: App(context).appWidth(40),
        height: App(context).appHeight(18),
        child: Card(
          shadowColor: Colors.grey,
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.white70, width: 1),
            borderRadius: BorderRadius.circular(15),
          ),
          elevation: 18,
          color: Colors.white,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [Color(0xFFEEEEEE), Colors.white]),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 2.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Image.asset(
                        "assets/images/cm0.jpeg",
                        width: 80,
                        height: 80,
                      )),
                  Container(
                      width: App(context).appWidth(40),
                      height: App(context).appHeight(5),
                      padding: EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Color(0xff11b461),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(15),
                            bottomRight: Radius.circular(15)),
                      ),
                      // margin: EdgeInsets.only(, top: 0),
                      child: Text(
                        headingText,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
