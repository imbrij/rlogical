

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const headerTextForIdentification = TextStyle(
    color: Colors.black,
    fontSize: 18,
    );

const subTitleHeader = TextStyle(
    color: Colors.grey,
    fontSize: 14,
    );