import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:xml2json/xml2json.dart';

import 'models/calenderPojo.dart';

class CheckCalender extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CheckCalender();
}

class _CheckCalender extends State<CheckCalender> {
  Xml2Json xml2json = new Xml2Json();
  Calender calender = Calender();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData().then((value) {
      setState(() {
        calender = value;
      });
      print(
          "Data: ${calender.data.bookedEventSessionDateListResModel[0].endDateTime}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: AppBar(
            elevation: 0.0,
            automaticallyImplyLeading: false,
            backgroundColor: Colors.transparent,
            title: Container(
              padding: EdgeInsets.only(top: 20),
            ),
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Text(
              "End Data: ${calender.data != null ? calender.data.bookedEventSessionDateListResModel[0].endDateTime : ""}"),
        ),
      ),
    );
  }

  Future<Calender> getData() async {
    final response = await http.get(
        'http://developer.kuwaiterp.com/ERPMobileAPI/api/event/GetBookedEventSessionList/17/64474/10192');
    if (response.statusCode == 200) {
      return Calender.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load post');
    }
  }
}
